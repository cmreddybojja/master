import { Injectable } from '@angular/core';
import { Feedback, ContactType } from '../shared/feedback';
import { map,catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { Observable, of } from 'rxjs';


import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

    constructor( private http: HttpClient ,private processHTTPMsgService: ProcessHTTPMsgService ) { }
  
  putFeedBack(feedBack: Feedback): Observable<Feedback> {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      };
      console.log(feedBack);
      return this.http.post<Feedback>(baseURL + 'feedback' , feedBack, httpOptions)
        .pipe(catchError(this.processHTTPMsgService.handleError));

    }
}
