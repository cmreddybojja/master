import { Component, OnInit, Input,Inject } from '@angular/core';
import { CorporateLeadersService } from '../services/corporate-leaders.service';
import { Leader } from '../shared/leader';

@Component( {
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
} )
export class AboutComponent implements OnInit {
    leader: Leader;
    leaders: Leader[];
    errMess: string;
    constructor( private leaderService: CorporateLeadersService,
            @Inject( 'BaseURL' ) private BaseURL) { }

    ngOnInit() {
        this.leaderService.getLeaders().subscribe(leaders => this.leaders = leaders, errmess => this.errMess = <any>errmess);
    }

}
