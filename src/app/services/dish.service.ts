import { Injectable } from '@angular/core';

//import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { Observable, of } from 'rxjs';

import { Dish } from '../shared/dish';


import { map,catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';


import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable( {
    providedIn: 'root'
} )
export class DishService {

    /*Getting the details from json-server using HttpClient*/
    constructor( private http: HttpClient ,private processHTTPMsgService: ProcessHTTPMsgService ) { }

    getDishes(): Observable<Dish[]> {
        return this.http.get<Dish[]>( baseURL + 'dishes' ).pipe(catchError(this.processHTTPMsgService.handleError));
    }

    getDish( id: number ): Observable<Dish> {
        return this.http.get<Dish>( baseURL + 'dishes/' + id ).pipe(catchError(this.processHTTPMsgService.handleError));
    }

    getFeaturedDish(): Observable<Dish> {
        return this.http.get<Dish[]>( baseURL + 'dishes?featured=true' ).pipe( map( dishes => dishes[0] ) ).pipe(catchError(this.processHTTPMsgService.handleError));
    }

    getDishIds(): Observable<number[] | any> {
        return this.getDishes().pipe( map( dishes => dishes.map( dish => dish.id ) ) ).pipe(catchError(this.processHTTPMsgService.handleError));
    }
    
    putDish(dish: Dish): Observable<Dish> {
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
          })
        };
        return this.http.put<Dish>(baseURL + 'dishes/' + dish.id, dish, httpOptions)
          .pipe(catchError(this.processHTTPMsgService.handleError));

      }
    /*Retuning the Observable inplace of Promise, 
     * So we need to handle this, where ever we are calling*/
    /*getDishIds(): Observable<string[] | any> {
        return of( DISHES.map( dish => dish.id ) );
    }*/

    /*getDishes(): Observable<Dish[]> {
        return of(DISHES).pipe(delay(2000));
      }
  
      getDish(id: string): Observable<Dish> {
          return of(DISHES.filter((dish) => (dish.id === id))[0]).pipe(delay(2000));
      }
  
      getFeaturedDish(): Observable<Dish> {
        return of(DISHES.filter((dish) => dish.featured)[0]).pipe(delay(2000));
      }*/

    /*Creating Observables and converting to Promises*/

    /*  getDishes(): Promise<Dish[]> {
          return of(DISHES).pipe(delay(2000)).toPromise();
        }
    
        getDish(id: number): Promise<Dish> {
          return of(DISHES.filter((v) => (dish.id === id))[0]).pipe(delay(2000)).toPromise();
        }
    
        getFeaturedDish(): Promise<Dish> {
          return of(DISHES.filter((dish) => dish.featured)[0]).pipe(delay(2000)).toPromise();
        }*/

    /*This is with Promise*/
    /*getDishes(): Promise<Dish[]>{
        return new Promise(resolve=> {
            // Simulate server latency with 2 second delay
              setTimeout(() => resolve(DISHES), 2000);  
        });
        //Promise.resolve(DISHES);
    }
    
    getDish(id: string): Promise<Dish> {
        return new Promise(resolve=> {
            // Simulate server latency with 2 second delay
              setTimeout(() => resolve(DISHES.filter((dish) => (dish.id === id))[0]), 2000);
          }); 
        //return  Promise.resolve(DISHES.filter((dish) => (dish.id === id))[0]);
      }
  
      getFeaturedDish(): Promise<Dish> {
          return new Promise(resolve => 
          {
              setTimeout(() => resolve(DISHES.filter((dish) => dish.featured)[0]), 2000);
          });
        //return  Promise.resolve(DISHES.filter((dish) => dish.featured)[0]);
      }*/
}
