import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
//import { LEADERS } from '../shared/leaders';


import { delay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { map,catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';


import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable( {
    providedIn: 'root'
} )
export class CorporateLeadersService {

    /*Getting the details from json-server using HttpClient*/
    constructor( private http: HttpClient ,private processHTTPMsgService: ProcessHTTPMsgService ) { }
    
    getLeaders(): Observable<Leader[]> {
        
        return this.http.get<Leader[]>( baseURL + 'leadership' ).pipe(catchError(this.processHTTPMsgService.handleError));
        
        //return of( LEADERS ).pipe(delay(2000));
        // return Promise.resolve(LEADERS);
    }

    getLeader( id: string ): Observable<Leader> {
        
        return this.http.get<Leader>( baseURL + 'leadership/' + id ).pipe(catchError(this.processHTTPMsgService.handleError));
        //return of(LEADERS.filter(( leder ) => ( leder.id === id ) )[0]).pipe(delay(2000));
        // return Promise.resolve(LEADERS.filter(( leder ) => ( leder.id === id ) )[0]);
    }

    getFeaturedLeader(): Observable<Leader> {
        
        return this.http.get<Leader[]>( baseURL + 'leadership?featured=true' ).pipe( map( leadership => leadership[0] ) ).pipe(catchError(this.processHTTPMsgService.handleError));
        //return of(LEADERS.filter(( leader ) => ( leader.featured ) )[0]).pipe(delay(2000));
    }

    
    

   /* getLeaders(): Promise<Leader[]> {
        return new Promise( resolve => {
            setTimeout(() => resolve( LEADERS ), 2000 );
        } );
        // return Promise.resolve(LEADERS);
    }

    getLeader( id: string ): Promise<Leader> {
        return new Promise( resolve =>{ 
            setTimeout(() => resolve(LEADERS.filter(( leder ) => ( leder.id === id ) )[0]), 2000 );
    });
        // return Promise.resolve(LEADERS.filter(( leder ) => ( leder.id === id ) )[0]);
    }

    getFeaturedLeader(): Promise<Leader> {
        return new Promise( resolve =>{
            setTimeout(() => resolve(LEADERS.filter(( leader ) => ( leader.featured ) )[0]), 2000 );
    });
       // return Promise.resolve( LEADERS.filter(( leder ) => ( leder.featured ) )[0] );
    }*/
}
