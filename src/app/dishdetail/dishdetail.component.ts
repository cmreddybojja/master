import { Component, OnInit, Input,ViewChild, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Location, DatePipe } from '@angular/common';
import { MatSliderModule } from '@angular/material/slider'
import { switchMap } from 'rxjs/operators';

import { trigger, state, style, animate, transition } from '@angular/animations';


import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component( {
    selector: 'app-dishdetail',
    templateUrl: './dishdetail.component.html',
    animations: [
                 trigger('visibility', [
                     state('shown', style({
                         transform: 'scale(1.0)',
                         opacity: 1
                     })),
                     state('hidden', style({
                         transform: 'scale(0.5)',
                         opacity: 0
                     })),
                     transition('* => *', animate('0.5s ease-in-out'))
                 ])
               ],
    styleUrls: ['./dishdetail.component.scss']
} )
export class DishdetailComponent implements OnInit {

    @Input()
    dish: Dish;

    dishIds: string[];
    comments: Comment[];
    prev: string;
    next: string;
    @ViewChild('fform',{static: false}) commentsFormDirective;

    constructor( private dishService: DishService,
        private route: ActivatedRoute,
        private location: Location,
        private fb: FormBuilder,
        @Inject( 'BaseURL' ) private BaseURL) {
        this.createForm();
    }
    commentsForm: FormGroup;
    commentObj: Comment;
    dishcopy: Dish;
    createForm() {
        this.commentsForm = this.fb.group( {
            comment: ['', [Validators.required, Validators.minLength( 2 ), Validators.maxLength( 125 )]],
            rating: 5,
            author: ['', [Validators.required, Validators.minLength( 2 ), Validators.maxLength( 25 )]],
            date:'',
        } );

        this.commentsForm.valueChanges
            .subscribe( data => this.onValueChanged( data ) );

        this.onValueChanged();

    }
    
    onValueChanged(data?: any) {
        if (!this.commentsForm) { return; }
        const form = this.commentsForm;
        for (const field in this.formErrors) {
          if (this.formErrors.hasOwnProperty(field)) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            console.log(control);
            if (control && control.dirty && !control.valid) {
              const messages = this.validationMessages[field];
              for (const key in control.errors) {
                if (control.errors.hasOwnProperty(key)) {
                  this.formErrors[field] += messages[key] + ' ';
                }
              }
            }
          }
        }
      }


      onSubmit() {
        console.log(this.commentsForm.value);
        this.commentsForm.value['date']= new Date().toJSON("yyyy/MM/dd HH:mm");
        console.log(this.commentsForm.value);
        this.dish.comments.push(this.commentsForm.value);
        
        //this.dishcopy.comments.push(this.comment);putFeedBack
        this.dishService.putDish(this.dishcopy)
          .subscribe(dish => {
            this.dish = dish; this.dishcopy = dish;
          },
          errmess => { this.dish = null; this.dishcopy = null; this.errMess = <any>errmess; });
        
        
        
        this.commentsFormDirective.resetForm();
        this.commentsForm.reset({
          author: '',
          comment: '',
          rating: 5,
          date:'',
        });
        
      }


    formErrors = {
        'author': '',
        'comment': '',
        'rating': '',
        'date': '',
    };

    validationMessages = {
        'author': {
            'required': 'Author Name is required.',
            'minlength': 'Author Name must be at least 2 characters long.',
            'maxlength': 'Author cannot be more than 25 characters long.'
        },
        'comment': {
            'required': 'Comment is required.',
            'minlength': 'Comment must be at least 2 characters long.',
            'maxlength': 'Comment cannot be more than 25 characters long.'
        },
        'rating':  {
            'required': 'rating is required.'
        },
    };
    
    errMess: string;
    visibility = 'shown';

    ngOnInit() {
        this.dishService.getDishIds().subscribe( dishIds => this.dishIds = dishIds, errmess => this.errMess = <any>errmess);
        this.route.params.pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(+params['id']); }))
        .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
          errmess => this.errMess = <any>errmess);
    }

    setPrevNext( dishId: string ) {
        const index = this.dishIds.indexOf( dishId );
        this.prev = this.dishIds[( this.dishIds.length + index - 1 ) % this.dishIds.length];
        this.next = this.dishIds[( this.dishIds.length + index + 1 ) % this.dishIds.length];
    }
	/*ngOnInit() {
	    let id = this.route.snapshot.params['id'];
	    //This to handle Observable, if we are getting Observable as return type
        this.dishService.getDish(id).subscribe(dish => this.dish = dish);

	    //this.dish = this.dishService.getDish(id);
	    //This to handle Promise, if we are getting Promise as return type
	    //this.dishService.getDish(id).then(dish => this.dish = dish);
	}*/
    goBack() {
        this.location.back();
    }

}
